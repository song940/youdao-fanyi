# Youdao-fanyi

[![Build Status](https://travis-ci.org/song940/youdao-fanyi.svg?branch=master)](https://travis-ci.org/song940/youdao-fanyi)

[Youdao](http://fanyi.youdao.com) translate tools for nodejs .

[![NPM](https://nodei.co/npm/youdao-fanyi.png?downloads=true&stars=true)](https://nodei.co/npm/youdao-fanyi/)


## Installation

```bash
~$ [sudo] npm install youdao-fanyi [-g] [--save]
```

## Usage

```javascript
var youdao = require('youdao-fanyi')({
	key		: '2001075261',
	keyfrom	: 'LSONGORG'
});

youdao.fanyi('Hello World!', function(err, results, body){
	if(err)return console.error(err);
	console.log(results);
});
```

## Licence

The MIT License (MIT)

Copyright (c) 2014 Lsong

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
