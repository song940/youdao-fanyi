var http  = require('http');
var url   = require('url');

var Youdao = function(options){
  return Youdao.fanyi.init(options);
};

Youdao.fanyi = {
  init: function(options){
    this.api = url.parse("http://fanyi.youdao.com/openapi.do");
    this.api.query = {
      type    : 'data',
      doctype : 'json',
      version : '1.1',
      q       : '',
    };
    for(var key in options){
      this.api.query[ key ] = options[key];
    }
    return this;
  },
  fanyi: function(text, callback){
    this.api.query['q'] = text;
    http.get(url.format(this.api), function(res){
      var tmp = [];
      res.on('data', function(chunk){
        tmp.push(chunk);
      });
      res.on('end', function(){
        var data = tmp.join();
        try{
          var json = JSON.parse(data);
          callback(null, json.translation[0], json);
        }catch(e){
          e.data = data;
          callback(e);
        }
      });
    })
  }
};

module.exports = Youdao;
