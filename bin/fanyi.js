#!/usr/bin/env node
var youdao = require('../')({
  key			: '2001075261',
  keyfrom	: 'LSONGORG'
});

var parser = function(argv){
  return argv.slice(2).join(' ');
};

console.log('Youdao - fanyi:');
console.log('---------------');

youdao.fanyi(parser(process.argv), function(err, results, body){
  if(err)return console.error(err);
  body.translation.forEach(function(text){
    console.log('- %s', text);
  });
});
